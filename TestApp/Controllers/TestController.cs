﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace TestApp.Controllers
{
    public class LogInParameters
    {
        public string UserName { get;set;}
        public string Password { get; set; }
    }

    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        /// <summary>
        /// Тестовый метод
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<string> LogIn(LogInParameters param)
        {
            if (param == null)
            {
                return $"Parameter 'param' is null";
            }

            if (string.IsNullOrWhiteSpace(param.UserName))
            {
                return $"Parameter 'userName' is empty";
            }

            if (string.IsNullOrWhiteSpace(param.Password))
            {
                return $"Parameter 'password' is empty";
            }

            byte[] passwordHash;

            using (var algorithm = SHA256.Create())
            {
                passwordHash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(param.Password));
            }

            var sb = new StringBuilder();
            sb.AppendLine("");

            foreach (var item in passwordHash)
            {
                sb.Append(item.ToString("X2"));
            }

            return $"Hi, {param.UserName}, you are logged with password '{sb}'";
        }
    }
}